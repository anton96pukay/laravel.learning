<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;

class IndexController extends Controller
{
    protected $header;
    protected $message;

    public function __construct()
    {
        $this->header = 'Привіт світ';
        $this->message = 'This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.';
    }

    public function index(){

        $articles = Article::select(['id', 'title', 'desc'])->get();

        return view('page')->with([
            'message'   => $this->message,
            'header'    => $this->header,
            'articles'  => $articles
        ]);
    }

    public function show($id){

        $article = Article::select(['id', 'title', 'text'])->where('id', $id)->first();

        return view('article-content')->with([
            'message'   => $this->message,
            'header'    => $this->header,
            'article'  => $article
        ]);

    }

    public function add(){
        return view('add-content')->with([
            'message'   => $this->message,
            'header'    => $this->header,
            ]);
    }

    public function store(){

    }
}
