@extends('layouts.site')

@section('content')
<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1 class="display-3">{{$header}}</h1>
        <p>{{$message}}</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">

        @foreach($articles as $article)

            <div class="col-md-4">
                <h2>{{$article->title}}</h2>
                <p>{{$article->desc}}</p>
                <p><a class="btn btn-secondary" href="{{route('articleShow', ['id'=>$article->id])}}" role="button">Детальніше &raquo;</a></p>
            </div>


        @endforeach

    </div>

    <hr>

    <footer>
        <p>&copy; Anton Pukay //2017</p>
    </footer>
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="{{asset('js/assets/js/vendor/popper.min.js')}}"></script>
<script src="{{asset('js/dist/js/bootstrap.min.js')}}"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{asset('js/assets/js/ie10-viewport-bug-workaround.js')}}"></script>

@endsection